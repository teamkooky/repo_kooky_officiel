package com.example.clovi.cookyapp.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import butterknife.ButterKnife;
import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.clovi.cookyapp.Activity.PageRecherche;
import com.example.clovi.cookyapp.Objects.Recette;
import com.example.clovi.cookyapp.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by clovi on 07/04/2017.
 */

public class AdapterRecette extends RecyclerView.Adapter<AdapterRecette.ViewHolder>{


    List<Recette> mRecette;
    Context ctx;
    static String recupHref;
    TextView tvHref;





    public interface InterfaceForPageRecherche{
            public void oneToast();
    }


    public AdapterRecette(Context ctx, List<Recette> recettes){
        this.ctx = ctx;
        this.mRecette = recettes;
    }



    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_recyclerview,parent,false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {


        Recette recette = mRecette.get(position);
        holder.textView.setText(recette.getTitle());
        tvHref.setText(" "+recupHref);
        recupHref = recette.getHref();
        Glide.with(ctx).load(recette.getThumbnail()).into(holder.imageView);
        if (holder.imageView == null){
            holder.textView2.setText("Image non disponible");
        }
    }

    @Override
    public int getItemCount() {
        return mRecette.size();
    }

    //class
    class ViewHolder extends RecyclerView.ViewHolder{

        /*@BindView(R.id.cardViewRecyclerView)
        CardView cardViewRecyclerView;*/
        TextView textView;
        TextView textView2;
        ImageView imageView;

        // CardView cardView;




        public ViewHolder(View itemView) {
            super(itemView);
            //ButterKnife.bind(this,itemView);
            textView = (TextView) itemView.findViewById(R.id.textViewRecyclerView);
            imageView = (ImageView) itemView.findViewById(R.id.ivRecyclerView);
            tvHref = (TextView) itemView.findViewById(R.id.tvHref);





            //cardView = (CardView) itemView.findViewById(R.id.cardView);



        }

       /* @BindView(R.id.textViewRecyclerView)
        TextView textViewRecyclerView;*/
    }
}
