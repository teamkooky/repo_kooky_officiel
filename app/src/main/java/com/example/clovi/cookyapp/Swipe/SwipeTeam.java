package com.example.clovi.cookyapp.Swipe;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.clovi.cookyapp.Fragment.Presentation_Team_1;
import com.example.clovi.cookyapp.Fragment.Presentation_Team_2;

/**
 * Created by Ä.M.E on 15/03/2017.
 */

public class SwipeTeam extends FragmentPagerAdapter {
    public String fragments[] = {"fragment 1", "fragment 2", "fragment 3 ", "fragment 4"};

    public SwipeTeam(FragmentManager fm, Context ctx) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new Presentation_Team_1();
            case 1:
                return new Presentation_Team_2();
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return fragments.length;
    }
}
