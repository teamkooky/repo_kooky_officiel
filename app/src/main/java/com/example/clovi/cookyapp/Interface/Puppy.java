package com.example.clovi.cookyapp.Interface;

import com.example.clovi.cookyapp.Objects.ResponseRecette;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by clovi on 07/04/2017.
 */


public interface Puppy {
    //site solicité : http://www.recipepuppy.com/

    @GET("/api")
    Call<ResponseRecette> listRecette(@Query("q") String query,@Query("i") String ingredients,@Query("p") int page);
}
