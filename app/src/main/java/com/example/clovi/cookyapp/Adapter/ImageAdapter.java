package com.example.clovi.cookyapp.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.example.clovi.cookyapp.R;

/**
 * Created by clovi on 22/03/2017.
 */

public class ImageAdapter extends BaseAdapter {
    private Context mContext;
    public int mPosition ;
    private LayoutInflater inflater;
    public View mVu;


    public int [] images= {R.drawable.lait, R.drawable.oeuf, R.drawable.farine, R.drawable.tomate, R.drawable.steak,
            R.drawable.pomme_de_terre, R.drawable.persil, R.drawable.poulet_poitrine, R.drawable.poulet_pilon, R.drawable.poulet_aile, R.drawable.pate, R.drawable.riz, R.drawable.pain, R.drawable.oignon, R.drawable.poivron_vert, R.drawable.poivron_rouge, R.drawable.poivron_jaune};
    public ImageAdapter(Context c, int nPosition, View nVu)
    {
        mContext = c;
        mPosition = nPosition;
        mVu = nVu;

    }
    @Override
    public int getCount() {

        return images.length;
    }

    @Override
    public Object getItem(int a) {
        return null;
    }

    @Override
    public long getItemId(int b) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        position = mPosition;

        // ImageView imageView;
        View imageView = convertView;
        mVu =imageView;

        if (convertView == null) {
            // if it's not recycled, initialize some attributes
            // imageView = new SingleItem(mContext);
            // imageView.setLayoutParams(new GridView.LayoutParams(85, 85));
            // imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            // imageView.setPadding(8, 8, 8, 8);

            inflater = (LayoutInflater) mContext.getSystemService(mContext.LAYOUT_INFLATER_SERVICE);
            imageView =inflater.inflate(R.layout.item_single, null);
        }
        //else {
        // imageView = (ImageView) convertView;
        //}
        // imageView.setImageResource(Image[position]);
        ImageView image = (ImageView) imageView.findViewById(R.id.icon);
        image.setImageResource(images[position]);

        return imageView;

    }
}
