package com.example.clovi.cookyapp.Activity;


import android.content.Intent;
import android.net.Uri;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.MediaController;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;


import com.example.clovi.cookyapp.Adapter.AdapterRecette2;
import com.example.clovi.cookyapp.R;
import com.example.clovi.cookyapp.Swipe.SwipeTeam;

import butterknife.BindView;
import butterknife.OnClick;


public class PremierePage extends AppCompatActivity implements View.OnClickListener{

    ViewPager viewPager;
    AdapterRecette2 adapter;
    VideoView videoView;
    MediaController mediaController;
    private View v;
    public Spinner spinner_entree;
    public Spinner spinner_plat;
    public Spinner spinner_dessert;
    public ArrayAdapter<CharSequence> adapterTextView;
    TextView tv;
    ImageButton bRechercher;
    String motClef;
    TextView laTeam;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_premiere_page);
        videoView = (VideoView)findViewById(R.id.videoView);
        mediaController = new MediaController(this);
        viewPagerRecette();
        videoRecette(v);
        bouttonEPD();
        bRechercher.setOnClickListener(this);
        laTeam.setOnClickListener(this);





    }



    //fonction qui lance la video a l'ouverture de l'application
    public void videoRecette(View v){
        String videopath = "android.resource://com.example.clovi.cookyapp/"+R.raw.pouletaunoix;
        Uri uri = Uri.parse(videopath);
        videoView.setVideoURI(uri);
        videoView.setMediaController(mediaController);
        mediaController.setAnchorView(videoView);
        videoView.start();
        correspondance();


    }

    //E: Entrée, P: Plât, D: Dessert --> fonction qui gere les spinners, fenettre de menus
    public void bouttonEPD(){
                spinner_entree = (Spinner) findViewById(R.id.spinner_entree);
                adapterTextView = ArrayAdapter.createFromResource(PremierePage.this, R.array.entree_names, android.R.layout.simple_spinner_item);
                adapterTextView.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinner_entree.setAdapter(adapterTextView);
                spinner_entree.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        motClef = (String) parent.getItemAtPosition(position);
                        if (!motClef.isEmpty()){
                            Intent intent = new Intent(getApplicationContext(), ResultSearch.class);
                            intent.putExtra("motclef",motClef);
                            spinner_entree.setAdapter(null);
                            startActivity(intent);
                        }

                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });

                spinner_plat = (Spinner) findViewById(R.id.spinner_plat);
                adapterTextView = ArrayAdapter.createFromResource(PremierePage.this, R.array.plat_names, android.R.layout.simple_spinner_item);
                adapterTextView.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinner_plat.setAdapter(adapterTextView);
                spinner_plat.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        motClef = (String) parent.getItemAtPosition(position);
                        if (!motClef.isEmpty()){
                            Intent intent = new Intent(getApplicationContext(), ResultSearch.class);
                            intent.putExtra("motclef",motClef);
                            spinner_plat.setAdapter(null);
                            startActivity(intent);
                        }

                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });


                spinner_dessert = (Spinner) findViewById(R.id.spinner_dessert);
                adapterTextView = ArrayAdapter.createFromResource(PremierePage.this, R.array.dessert_names, android.R.layout.simple_spinner_item);
                adapterTextView.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinner_dessert.setAdapter(adapterTextView);
                spinner_dessert.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        motClef = (String) parent.getItemAtPosition(position);
                        if (!motClef.isEmpty()){
                            Intent intent = new Intent(getApplicationContext(), ResultSearch.class);
                            intent.putExtra("motclef",motClef);
                            spinner_dessert.setAdapter(null);
                            startActivity(intent);
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });

    }

    //fonction qui met en place le slide des ingredients proche de  de la video
    public void viewPagerRecette(){
        viewPager = (ViewPager) findViewById(R.id.view_pager);
        adapter = new AdapterRecette2(this);
        viewPager.setAdapter(adapter);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case (R.id.id_bouton_recherche):
                startActivity(new Intent(this, PageRecherche.class));
                break;
            case (R.id.tv_team):
                startActivity(new Intent(this,TeamPresentation.class));
                break;


        }
    }
    public void correspondance(){
        bRechercher = (ImageButton) findViewById(R.id.id_bouton_recherche);
        laTeam = (TextView) findViewById(R.id.tv_team);
    }
}
