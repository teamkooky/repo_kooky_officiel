package com.example.clovi.cookyapp.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;

import android.app.Activity;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.Toast;

import static android.widget.Toast.LENGTH_LONG;

import com.example.clovi.cookyapp.Activity.PageRecherche;
import com.example.clovi.cookyapp.Activity.ResultSearch;
import com.example.clovi.cookyapp.Adapter.ImageAdapter;
import com.example.clovi.cookyapp.R;

/**
 * Created by clovi on 22/03/2017.
 */

public class RechercheParIngredients extends Fragment {
    private View vueRechercheI;
    ImageView imageView ;
    ImageView imageView2;
    ImageView imageView3;
    ImageView imageView4;
    ImageView imageView5;
    ImageView imageView6;
    AutoCompleteTextView autoCompleteTextView ;
    public  int nbIngredient =0;
    public String selection;
    String[] ingComplet = new String[9];
    public String ingredientComplet = "";
    private Button brechercher;
    String name;
    public int numberDelete;

    public String[] ingredientsCli = new String[9];


    public int[] Image = {R.drawable.milk, R.drawable.eggs, R.drawable.flour, R.drawable.tomato, R.drawable.steack,
            R.drawable.potatoes, R.drawable.parsley, R.drawable.chicken_breast, R.drawable.chicken_drumsticks, R.drawable.chicken_wings, R.drawable.pasta, R.drawable.rice, R.drawable.bread, R.drawable.onions, R.drawable.green_pepper,
            R.drawable.red_pepper, R.drawable.yellow_pepper, R.drawable.asparagus, R.drawable.mustard, R.drawable.banana, R.drawable.apple, R.drawable.orange_fruit, R.drawable.lime, R.drawable.red_onions, R.drawable.mint, R.drawable.teriyaki,
            R.drawable.spinach, R.drawable.wasabi, R.drawable.ginger, R.drawable.paprika, R.drawable.pumpkin_puree, R.drawable.sugar, R.drawable.guacamole, R.drawable.carrot, R.drawable.lobsters, R.drawable.crabs, R.drawable.shrimp,
            R.drawable.peanuts_butter, R.drawable.peanuts, R.drawable.vegetable_oil, R.drawable.soy_sauce, R.drawable.cilantro, R.drawable.canola_oil, R.drawable.flour_tortillas, R.drawable.fish, R.drawable.salmon, R.drawable.mayo, R.drawable.ketchup, R.drawable.fries,
            R.drawable.avocado, R.drawable.sesame_seeds, R.drawable.vanilla, R.drawable.chocolate, R.drawable.cucumber, R.drawable.vinegar, R.drawable.red_wine_vinegar, R.drawable.salad_greens, R.drawable.mushroom, R.drawable.green_peas,
            R.drawable.gruyere_cheese, R.drawable.mozzarella_cheese, R.drawable.oregano, R.drawable.cottage_cheese, R.drawable.salt, R.drawable.lemon, R.drawable.eggplant, R.drawable.coriander, R.drawable.caraway_seeds, R.drawable.basil,
            R.drawable.brocoli, R.drawable.bacon, R.drawable.butter, R.drawable.black_pepper, R.drawable.parmesan_cheese, R.drawable.olive, R.drawable.zuchinni, R.drawable.olive_oil, R.drawable.garlic};

    String[] Ingredient_Names = {"Milk", "Eggs","Flour", "Tomato", "Steak",  "Potatoes", "Parsley", "Chicken-Breast", "Chicken-drumstick", "Chicken-wings", "Pasta", "Rice", "Bread", "Onions", "Green-pepper","Red-pepper","Yellow-pepper",
            "Asparagus", "Mustard", "Banana", "Apple", "Orange", "lime", "Red-onions", "Mint", "Teriyaki", "Spinach", "Wasabi", "Ginger", "Paprika", "Pumpkin", "Sugar", "Guacamole", "Carrot", "Lobster", "Crab", "Shrimp", "Peanut-butter",
            "Peanuts", "Vegetable-Oil", "Soy-Sauce", "Cilantro", "Canola-Oil", "Tortillas", "Fish", "Salmon", "Mayo", "Ketchup", "Fries", "Avocado", "Sesame_Seeds", "Vanilla", "Chocolate", "Cucumber", "Vinegar", "Red-Wine-Vinegar", "Salad-Green",
            "Mushrooms", "Green-Peas", "Gruyere-Cheese", "Mozzarella-cheese","Oregano","Cottage-cheese",  "Salt", "Lemon", "Eggplant", "Coriander", "Caraway-seeds", "Basil", "Broccoli", "Bacon", "Butter", "Black-pepper", "Parmesan-cheese", "Olive", "Zucchini", "Olive-oil", "Garlic"};



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        vueRechercheI = inflater.inflate(R.layout.fragment_ingredients_par_recherche,container,false);
        correspondence();
        autoCompleteMoiSa();
        putImage();
        launchRecherche();

        options(imageView);
        options(imageView2);
        options(imageView3);
        options(imageView4);
        options(imageView5);
        options(imageView6);

        return vueRechercheI;

    }

    public void correspondence()
    {
        imageView = (ImageView) vueRechercheI.findViewById(R.id.imageView);
        imageView2 = (ImageView) vueRechercheI.findViewById(R.id.imageView2);
        imageView3 = (ImageView) vueRechercheI.findViewById(R.id.imageView3);
        imageView4 = (ImageView) vueRechercheI.findViewById(R.id.imageView4);
        imageView5 = (ImageView) vueRechercheI.findViewById(R.id.imageView5);
        imageView6 = (ImageView) vueRechercheI.findViewById(R.id.imageView6);
        brechercher = (Button) vueRechercheI.findViewById(R.id.button_rechercher);
    }
    public void autoCompleteMoiSa()
    {
        autoCompleteTextView = (AutoCompleteTextView) vueRechercheI.findViewById(R.id.ingredient);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, Ingredient_Names);
        autoCompleteTextView.setAdapter(adapter);
    }
    public void putImage(){

        autoCompleteTextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                //Log.d("arg1", arg1.toString());
                for (int i = 0; i <= Ingredient_Names.length - 1; i++) {
                     selection = (String) arg0.getItemAtPosition(arg2);//La variable String contient l'element que l'utilisateur à taper
                    if (Ingredient_Names[i] == selection) {
                        if (imageView.getDrawable() == null){
                            imageView.setImageResource(Image[i]);
                            imageView.setTag("imageView");

                        }else{
                            if (imageView2.getDrawable() == null){
                                imageView2.setImageResource(Image[i]);
                                imageView2.setTag("imageView2");

                            }else{
                                if (imageView3.getDrawable() == null){
                                    imageView3.setImageResource(Image[i]);
                                    imageView3.setTag("imageView3");

                                }else{
                                    if (imageView4.getDrawable() == null){
                                        imageView4.setImageResource(Image[i]);
                                        imageView4.setTag("imageView4");

                                    }else{
                                        if (imageView5.getDrawable() == null){
                                            imageView5.setImageResource(Image[i]);
                                            imageView5.setTag("imageView5");

                                        }else {
                                            if (imageView6.getDrawable() == null){
                                                imageView6.setImageResource(Image[i]);
                                                imageView6.setTag("imageView6");

                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                }

                nbIngredient +=1;
                ingredientComplet += selection+",";


               /* for(int j=nbIngredient;j< nbIngredient+1;j++){
                    ingComplet[nbIngredient] = selection;
                   // ingredientComplet += ingComplet[nbIngredient] + ",";
                   // Toast.makeText(getActivity(), ingComplet[nbIngredient], Toast.LENGTH_SHORT).show();

                    autoCompleteTextView.setText("");

                }*/

                Toast.makeText(getActivity(), "" + ingredientComplet, Toast.LENGTH_SHORT).show();


            }


        });}

    public void launchRecherche(){
        brechercher.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), ResultSearch.class);
                intent.putExtra("ingredientComplet",ingredientComplet);
                startActivity(intent);

            }
        });
    }
    public void options(final ImageView image) { /* J'ai essayer de faire en sorte que quand on click sur l'image, qu'on puisse
                                            changer l'ingrédient où le supprimer, mais l'appli s'arrete quand j'essai la méthode, jette y un oeil stp*/
        image.setOnClickListener(new View.OnClickListener() {
            String titre;
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(getContext(), image);
                popupMenu.getMenuInflater().inflate(R.menu.main_context_menu, popupMenu.getMenu());
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {

                        titre = (String) menuItem.getTitle();
                        name = (String) image.getTag();
                        //Toast.makeText(getActivity(), " "+ image.getTag(), Toast.LENGTH_SHORT).show();
                        //Toast.makeText(getContext(), "" + titre, Toast.LENGTH_SHORT).show();
                        if (titre == "Delete" ){
                            image.setImageDrawable(null);
                            for (int i=0;i <ingComplet.length;i++){
                                if (name == ingComplet[i]){
                                    ingComplet[i] = null;
                                }
                            }



                        } else {
                            autoCompleteTextView.setText("");
                            image.setImageDrawable(null);
                            autoCompleteMoiSa();
                        }
                        return true;
                    }


                });
                popupMenu.show();

            }

        });

    }
}

