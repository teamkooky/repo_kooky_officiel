package com.example.clovi.cookyapp.Activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.clovi.cookyapp.Adapter.AdapterRecette;
import com.example.clovi.cookyapp.Fragment.RechercheGenerale;
import com.example.clovi.cookyapp.Interface.Puppy;
import com.example.clovi.cookyapp.Objects.Recette;
import com.example.clovi.cookyapp.Objects.ResponseRecette;
import com.example.clovi.cookyapp.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by clovi on 12/04/2017.
 */

public class ResultSearch extends Activity implements View.OnClickListener {

    @BindView(R.id.recyclerViewRecette)
    public RecyclerView rv;
    private AdapterRecette adapterRecette;
    public RechercheGenerale rechercheGenerale = new RechercheGenerale();
    public String query;
    public String ingredients;
    static int page = 1;
    private ImageView flecheDroite;
    private ImageView flecheGauche;
    private TextView tvPage;



    @Override
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_result);
        correspondance();
        Intent intent = getIntent();
        query = intent.getStringExtra("motclef");
        ingredients = intent.getStringExtra("ingredientComplet");
        Toast.makeText(this, "mot clef :"+query, Toast.LENGTH_SHORT).show();
        flecheDroite.setOnClickListener(this);
        flecheGauche.setOnClickListener(this);
        tvPage.setText(""+page);
        Toast.makeText(this, " "+ingredients, Toast.LENGTH_SHORT).show();


        ButterKnife.bind(this);


        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("http://www.recipepuppy.com/")
                .build();

        Puppy client = retrofit.create(Puppy.class);

        Call<ResponseRecette> call = client.listRecette(query,ingredients, page);

        call.enqueue(new Callback<ResponseRecette>() {
            @Override
            public void onResponse(Call<ResponseRecette> call, Response<ResponseRecette> response) {
                if(response.isSuccessful()){
                    List<Recette> recettes = response.body().getResults();
                    Toast.makeText(ResultSearch.this, "success", Toast.LENGTH_SHORT).show();
                    showListRecette(recettes);

                }

            }

            @Override
            public void onFailure(Call<ResponseRecette> call, Throwable t) {
                Toast.makeText(ResultSearch.this, "erreur", Toast.LENGTH_SHORT).show();

            }
        });



    }

    private void correspondance(){
        flecheGauche = (ImageView) findViewById(R.id.ivGauche);
        flecheDroite = (ImageView) findViewById(R.id.ivDroite);
        rv = (RecyclerView) findViewById(R.id.recyclerViewRecette);
        tvPage = (TextView) findViewById(R.id.tvPage);
    }


    public void showListRecette(List<Recette> recettes){
        adapterRecette = new AdapterRecette(getApplicationContext(), recettes);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        rv.setLayoutManager(mLayoutManager);
        rv.setItemAnimator(new DefaultItemAnimator());

        rv.setAdapter(adapterRecette);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case (R.id.ivDroite):
                page += 1;
                Toast.makeText(this, "n :"+page, Toast.LENGTH_SHORT).show();
                finish();
                startActivity(getIntent());
                break;
            case (R.id.ivGauche):
                page -= 1;
                Toast.makeText(this, "n :"+page, Toast.LENGTH_SHORT).show();
                finish();
                startActivity(getIntent());
                break;
        }
    }
}
