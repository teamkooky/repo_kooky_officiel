package com.example.clovi.cookyapp.Adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.clovi.cookyapp.R;


/**
 * Created by clovi on 12/02/2017.
 */

public class AdapterRecette2 extends PagerAdapter {
    //definir un tableau d'image
    private int[] tab_recette={R.drawable.cuissepoulet, R.drawable.noixcajou, R.drawable.miel, R.drawable.saucesoja};
    //deinir un context
    private Context ctx;
    //deinir un LayoutInflater
    private LayoutInflater layoutInflater;

    //faire un constructeur avec en paremetre le context
    public AdapterRecette2(Context ctx){
        this.ctx = ctx;
    }
    @Override
    public int getCount() {
        return tab_recette.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return (view == (LinearLayout)object);
    }
    //préparer l'instanciation
    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        //mettre en place me contexte
        layoutInflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        //donner une vue a l'adapter
        View item_view = layoutInflater.inflate(R.layout.swipe_entree_fr, container, false);
        //relier les images avec les "vues images"
        ImageView imageView = (ImageView) item_view.findViewById(R.id.image_view);
        TextView textView =(TextView) item_view.findViewById(R.id.image_count);
        //preciser l'emplacement des images
        imageView.setImageResource(tab_recette[position]);
        //indiquer le contenu de image_count
        textView.setText("image :"+position);
        //ajouter la vue crée
        container.addView(item_view);
        //retourner la vue
        return item_view;
    }


    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout)object);
    }
}
