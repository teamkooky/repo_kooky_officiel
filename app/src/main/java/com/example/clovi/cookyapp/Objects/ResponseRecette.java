package com.example.clovi.cookyapp.Objects;


import java.util.List;

/**
 * Created by clovi on 07/04/2017.
 */

public class ResponseRecette {

    String title;

    List<Recette> results;

    public String getTitle() {
        return title;
    }

    public List<Recette> getResults() {
        return results;
    }


}
