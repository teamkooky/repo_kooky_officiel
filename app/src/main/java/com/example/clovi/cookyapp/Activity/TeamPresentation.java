package com.example.clovi.cookyapp.Activity;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.example.clovi.cookyapp.R;
import com.example.clovi.cookyapp.Swipe.SwipeTeam;


public class TeamPresentation extends AppCompatActivity {

    TabLayout tableLayout;
    ViewPager viewPager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_team);

        correspondance();
        viewPager = (ViewPager) findViewById(R.id.id_view_pager);
        tableLayout = (TabLayout) findViewById(R.id.tabs);
        tableLayout.setupWithViewPager(viewPager);
        tableLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());

            }
        });
        appelImage();

    }

    private void correspondance(){



    }

    public void appelImage(){
        ViewPager viewPager = (ViewPager) findViewById(R.id.id_view_pager);
        viewPager.setAdapter(new SwipeTeam(getSupportFragmentManager(), getApplicationContext()));
    }








    }

