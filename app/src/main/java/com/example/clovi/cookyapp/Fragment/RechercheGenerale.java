package com.example.clovi.cookyapp.Fragment;




import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.clovi.cookyapp.Activity.PageRecherche;
import com.example.clovi.cookyapp.Activity.ResultSearch;
import com.example.clovi.cookyapp.Objects.Recette;
import com.example.clovi.cookyapp.R;


/**
 * Created by clovi on 21/03/2017.
 */

public class RechercheGenerale extends Fragment implements View.OnClickListener {
    private View vueRecherche;
    TextView tv;
    ViewPager mViewPager;
    EditText query;
    Recette recette;
    ListView listView;
    Button cParti;
    String motClef;






    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        vueRecherche = inflater.inflate(R.layout.fragment_globale_recherche, container, false);
        correspondance();
        tv.setOnClickListener(this);
        cParti.setOnClickListener(this);


        //mViewPager = (ViewPager) vueRecherche.findViewById(R.id.vpRecherche);

        return vueRecherche;
    }
    public void correspondance(){
        tv = (TextView) vueRecherche.findViewById(R.id.ingredient_connu);
        query = (EditText) vueRecherche.findViewById(R.id.etRechercheGoblale);
        cParti = (Button) vueRecherche.findViewById(R.id.bCparti);

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case(R.id.ingredient_connu):
                /*FragmentManager fm = getFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                Fragment ffrag = new RechercheParIngredients();
                ft.replace(R.id.activity_main,ffrag);
                ft.commit();

                je me demande pourquoi ca ne marche pas hein...
                */
                break;
            case (R.id.bCparti):
                motClef = query.getText().toString();
                Intent intent = new Intent(getContext(), ResultSearch.class);
                intent.putExtra("motclef",motClef);
                startActivity(intent);
                break;


        }
    }
    private int getItem(int i) {
        return mViewPager.getCurrentItem() + i;
    }


}


