package com.example.clovi.cookyapp.Service;

import com.example.clovi.cookyapp.Objects.Recette;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by clovi on 21/03/2017.
 */

public interface IHttpRecette {
    @GET("/api")
    public Call<List<Recette>> getRecette(@Query("q") String query,@Query("p") int page);



}
