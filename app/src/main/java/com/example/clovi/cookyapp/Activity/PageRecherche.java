package com.example.clovi.cookyapp.Activity;

import android.app.ActionBar;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;
import android.widget.Toast;

import com.example.clovi.cookyapp.Adapter.AdapterRecette;
import com.example.clovi.cookyapp.R;
import com.example.clovi.cookyapp.Swipe.SwipeFragmentRecherche;

/**
 * Created by clovi on 21/03/2017.
 */

public class PageRecherche extends AppCompatActivity  {

    Context ctx;

    String mRecupHref;
    Intent mIntent;



    TabLayout tab;
    ViewPager vp ;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_page_recherche);

        vp =(ViewPager) findViewById(R.id.vpRecherche);



        tab = (TabLayout) findViewById(R.id.tabs);
        tab.setupWithViewPager(vp);
        tab.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                vp.setCurrentItem(tab.getPosition());

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                vp.setCurrentItem(tab.getPosition());

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                vp.setCurrentItem(tab.getPosition());

            }
        });

        appelSwipe0();

    }

    public void appelSwipe0(){
        ViewPager viewPager = (ViewPager) findViewById(R.id.vpRecherche);
        viewPager.setAdapter(new SwipeFragmentRecherche(getSupportFragmentManager(), getApplicationContext()));
        viewPager.setCurrentItem(0);
    }
    public void appelSwipe1(){
        ViewPager viewPager1 = (ViewPager) findViewById(R.id.vpRecherche);
        viewPager1.setAdapter(new SwipeFragmentRecherche(getSupportFragmentManager(), getApplicationContext()));
        viewPager1.setCurrentItem(1);
    }





}
