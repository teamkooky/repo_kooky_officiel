package com.example.clovi.cookyapp.Objects;


/**
 * Created by clovi on 07/04/2017.
 */

public class Recette {
    private String title;
    private String thumbnail;
    private String href;

    public String getThumbnail(){
        return thumbnail;
    }

    public String getHref(){
        return href;
    }

    public String getTitle(){
        return title;
    }
}
