package com.example.clovi.cookyapp.Swipe;


import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.clovi.cookyapp.Fragment.RechercheGenerale;
import com.example.clovi.cookyapp.Fragment.RechercheParIngredients;
import com.example.clovi.cookyapp.R;

/**
 * Created by clovis on 21/03/2017.
 */

public class SwipeFragmentRecherche extends FragmentPagerAdapter {

    public String pageRecherche [] = {"Recheche Globale","Recherche par ingrédient"};


    public SwipeFragmentRecherche(FragmentManager fm, Context ctx) {
        super(fm);
        //this.ctx = ctx;
    }


    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0 :
                return new RechercheGenerale();
            case 1:
                return new RechercheParIngredients();
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
       return pageRecherche.length;
    }







}
